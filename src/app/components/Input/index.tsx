import React, { FunctionComponent, MouseEvent, ChangeEvent, KeyboardEvent, RefObject } from 'react';
import { StyledInput } from './styles';

type inputProps = {
  id: string;
  type: 'text' | 'number' | 'password' | 'email';
  placeholder?: string;
  value?: string;
  defaultValue?: string;
  tabIndex?: number;
  inputRef?: RefObject<HTMLInputElement>;
  onClick?: (event: MouseEvent<HTMLInputElement>) => void;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  onKeyDown?: (event: KeyboardEvent<HTMLInputElement>) => void;
};

export const Input: FunctionComponent<inputProps> = ({
                                                       id,
                                                       type,
                                                       placeholder,
                                                       value,
                                                       defaultValue,
                                                       tabIndex,
                                                       inputRef,
                                                       onClick,
                                                       onChange,
                                                       onKeyDown,
                                                     }) => {
  return (
    <>
      <StyledInput
        id={id}
        type={type}
        placeholder={placeholder}
        value={value}
        defaultValue={defaultValue}
        tabIndex={tabIndex}
        ref={inputRef}
        onKeyDown={onKeyDown}
        onChange={onChange}
        onClick={onClick}
        autoComplete='off'
      />
    </>
  );
};

