import styled from 'styled-components/macro';

export const StyledInput = styled.input`
  width: 100%;
  padding: 0;
  border: 0;
  outline: 0;
  color: #4b5155;
  background-color: #fff;
  background-clip: padding-box;
`;
