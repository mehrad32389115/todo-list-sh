import React, { FunctionComponent, KeyboardEvent, ChangeEvent, useState } from 'react';
import { TodoListDataType } from '../../containers/HomePage/types';
import { Tree, TreeProps } from './Tree';
import { TreeWrapper } from './styled';

export type NodeItems = {
  nodeIndex: number;
  nodeObj: TodoListDataType;
}

export type NodeAndParentItems = {
  parentIndex: number;
  parentObj: TodoListDataType;
} & NodeItems

type MainTreeProps = {
  onCreate?: (nodeData: NodeItems | null, shortcut?: string) => void;
  onDelete?: (event: KeyboardEvent<HTMLInputElement>, nodeId: string) => void;
  onIndent?: (event: KeyboardEvent<HTMLInputElement>, nodeData: NodeItems, shortcut?: string) => void;
  onEdit?: (event: ChangeEvent<HTMLInputElement>, nodeId: string) => void;
}

export const MainTree: FunctionComponent<TreeProps & MainTreeProps> = ({
                                                                         data,
                                                                         onCreate,
                                                                         onEdit,
                                                                         onDelete,
                                                                         onIndent,
                                                                       }) => {

  const [nodeId, setNodeId] = useState('');

  const getDataParent = (array, nodeId, parentIndex = null, parentObj = null) => {
    for (const [key, entry] of array.entries()) {
      if (entry.id === nodeId) {
        return { parentIndex, parentObj };
      }
      const deeperParentId = getDataParent(entry.children, nodeId, key, entry);
      if (entry.children && deeperParentId) {
        return deeperParentId;
      }
    }
    return null;
  };

  const handleActionsByShortcut = (e: React.KeyboardEvent<HTMLInputElement>, nodeData: NodeItems): void => {
    const NodeAndParentData = { ...nodeData, ...getDataParent(data, nodeData.nodeObj.id) };
    switch (e.key) {
      case (e.ctrlKey && e.shiftKey && 'Delete') :
        if (onDelete)
          onDelete(e, nodeData.nodeObj.id);
        break;
      case 'Enter' :
        if (onCreate)
          onCreate(NodeAndParentData, 'Enter');
        break;
      case (e.shiftKey && 'Tab') :
        if (onIndent)
          onIndent(e, NodeAndParentData, 'Shift + Tab');
        break;
      case 'Tab' :
        if (onIndent)
          onIndent(e, NodeAndParentData, 'Tab');
        break;
    }
  };

  const handleCreateByPlus = (): void => {
    if (onCreate)
      onCreate(null);
  };

  return (
    <TreeWrapper>
      <Tree
        data={data}
        onShortcut={handleActionsByShortcut}
        onEdit={onEdit}
      />
      <span onClick={handleCreateByPlus}>+</span>
    </TreeWrapper>
  );
};

