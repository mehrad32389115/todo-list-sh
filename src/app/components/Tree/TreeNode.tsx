import React, { FunctionComponent, ChangeEvent, useEffect, useRef, RefObject } from 'react';
import { TodoListDataType } from '../../containers/HomePage/types';
import { Input } from '../Input';
import { Tree, TreeActions } from './Tree';

export type TreeNodeProps = {
  node: TodoListDataType;
  nodeKey: number;
}

export const TreeNode: FunctionComponent<TreeNodeProps & TreeActions> = ({ node, onEdit, nodeKey, onShortcut }) => {
  const hasChild: boolean = !!node.children;
  const inputRef: RefObject<HTMLInputElement> = useRef(null);
  const isFunctionEdit = (e: ChangeEvent<HTMLInputElement>, nodeId: string): void => {
    if (onEdit) onEdit(e, nodeId);
  };
  useEffect(() => {
    if (inputRef.current)
      inputRef.current.focus();
  }, []);
  return (
    <li>
      <div>
        <Input
          id={node.id}
          inputRef={inputRef}
          onKeyDown={(e) => onShortcut(e, { nodeIndex: nodeKey, nodeObj: node })}
          onChange={(e) => isFunctionEdit(e, node.id)}
          value={node.title}
          tabIndex={-1}
          type='text'
        />
      </div>
      {hasChild && (
        <Tree
          onEdit={onEdit}
          onShortcut={onShortcut}
          data={node.children}
        />
      )}
    </li>
  );
};
