import styled from 'styled-components/macro';

export const TreeWrapper = styled.div`
  display: inline-block;
  span {
    cursor: pointer;
    margin-left: 13px;
    color: #a9abad;
    border-radius: 100%;
    padding: 2px 6px;

    :hover {
      background: rgb(220, 224, 226);
    }
  }
`

export const StyledTree = styled.ul`
  padding-left: 20px;

  li {
    list-style: none;
    padding-top: 10px;
    color: #4b5155;

    ul {
      position: relative;

      :before {
        content: '';
        position: absolute;
        left: 3px;
        height: 100%;
        width: 1px;
        background: rgb(236, 238, 240);
      }
    }

    div {
      display: grid;
      grid-template-columns: auto 1fr;
      grid-gap: 0 15px;
      align-items: center;

      :before {
        content: '';
        width: 8px;
        height: 8px;
        border-radius: 100%;
        background: #4b5155;
      }
    }
  }
`;