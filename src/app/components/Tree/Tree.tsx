import React, { ChangeEvent, FunctionComponent, KeyboardEvent } from 'react';
import { TreeNode } from './TreeNode';
import { TodoListState } from '../../containers/HomePage/types';
import { NodeItems } from './index';
import { StyledTree } from './styled';

export type TreeActions = {
  onShortcut: (event: KeyboardEvent<HTMLInputElement>, nodeData: NodeItems) => void;
  onEdit?: (event: ChangeEvent<HTMLInputElement>, nodeId: string) => void;
}

export type TreeProps = {
  data: TodoListState['todoList'];
}

export const Tree: FunctionComponent<TreeProps & TreeActions> = ({ data, onShortcut, onEdit }) => {
  return (
    <StyledTree>
      {data.map((tree, key) => (
        <TreeNode
          onShortcut={onShortcut}
          onEdit={onEdit}
          key={tree.id}
          nodeKey={key}
          node={tree}
        />
      ))}
    </StyledTree>
  );
};