import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { PayloadDataType, TodoListState } from './types';

export const initialState: TodoListState = {
  todoList: [],
};

const todosSlice = createSlice({
  name: 'home',
  initialState,
  reducers: {
    setTodoList(state, action: PayloadAction<TodoListState['todoList']>) {
      const { payload } = action;
      state.todoList = payload;
    },

    getTodoList(state, action: PayloadAction<TodoListState['todoList']>) {
      const { payload } = action;
      state.todoList = payload
    },

    todoListActions(state, action: PayloadAction<PayloadDataType>) {},
  },
});

export const { actions, reducer, name: sliceKey } = todosSlice;
