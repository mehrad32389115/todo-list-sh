import { NodeAndParentItems } from '../../components/Tree';

export type TodoListDataType = {
  id: string;
  title: string;
  children: Array<TodoListDataType>;
};

export interface TodoListState {
  todoList: Array<TodoListDataType>;
}

type PayloadCreateData = {
  nodeObj?: TodoListDataType;
  startSplice?: number;
}

export type PayloadDataType = {
  nodeId?: string;
  title?: string;
  nodeData?: NodeAndParentItems;
  createData?: PayloadCreateData;
  action: 'create' | 'edit' | 'delete' | 'indent' | 'indentOutwards';
}
