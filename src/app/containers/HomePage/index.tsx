import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { translations } from 'locales/i18n';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { useDispatch, useSelector } from 'react-redux';
import { actions, reducer, sliceKey } from './slice';
import { todoListSaga } from './saga';
import { selectTodoList } from './selector';
import { MainTree } from '../../components/Tree';
import { Wrapper } from './styles';

export function HomePage() {
  const { t } = useTranslation();
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: todoListSaga });
  const dispatch = useDispatch();
  const treeData = useSelector(selectTodoList);

  const handleCreateNode = (nodeData, shortcut): void => {
    if (shortcut === 'Enter') {
      if (nodeData.nodeObj.children.length > 0) {
        dispatch(actions.todoListActions({
          createData: { nodeObj: nodeData.nodeObj, startSplice: 0 },
          action: 'create',
        }));
      } else {
        dispatch(actions.todoListActions({
          createData: { nodeObj: nodeData.parentObj, startSplice: nodeData.nodeIndex + 1 },
          action: 'create',
        }));
      }
    } else {
      dispatch(actions.todoListActions({ action: 'create' }));
    }
  };

  const handleEditNode = (e, nodeId): void => {
    e.preventDefault();
    dispatch(actions.todoListActions({ nodeId: nodeId, title: e.target.value, action: 'edit' }));
  };

  const handleDeleteNode = (e, nodeId): void => {
    e.preventDefault();
    dispatch(actions.todoListActions({ nodeId: nodeId, action: 'delete' }));
  };

  const handleIndentNode = (e, nodeData, shortcut): void => {
    e.preventDefault();
    if (shortcut === 'Tab') {
      dispatch(actions.todoListActions({ nodeData: nodeData, action: 'indent' }));
    } else if (shortcut === 'Shift + Tab') {
      dispatch(actions.todoListActions({ nodeData: nodeData, action: 'indentOutwards' }));
    }
  };

  useEffect(() => {
    dispatch(actions.getTodoList(JSON.parse(localStorage.getItem('treeData') || '[]')));
  }, []);

  return (
    <>
      <Helmet>
        <title>{t(translations.headerTitle)}</title>
        <meta name='description' content={t(translations.headerContent)} />
      </Helmet>
      <Wrapper>
        <h1>{t(translations.todoList)}</h1>
        <MainTree
          data={treeData}
          onCreate={handleCreateNode}
          onEdit={handleEditNode}
          onDelete={handleDeleteNode}
          onIndent={handleIndentNode}
        />
      </Wrapper>
    </>
  );
}
