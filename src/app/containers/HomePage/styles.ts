import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  width: 100%;
  max-width: 900px;
  margin: auto;
  font-family: sans-serif;

  h1 {
    text-align: center;
  }
`;
