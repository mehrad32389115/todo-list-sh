import { put, takeLatest, call, select } from 'redux-saga/effects';
import { actions } from './slice';
import { selectTodoList } from './selector';
import { PayloadDataType, TodoListDataType } from './types';
import { NodeAndParentItems } from '../../components/Tree';

const deepCreateNode = (data: Array<TodoListDataType>, objForSplice: TodoListDataType | undefined, startSplice: number) => {
  const generateId = (Math.random() + 1).toString(36).substring(7);
  const deepCloneData = JSON.parse(JSON.stringify(data));
  if (objForSplice) {
    return deepCloneData.map(item => {
      if (item.id === objForSplice.id) {
        item.children.splice(startSplice, 0, { id: generateId, title: '', children: [] });
      } else {
        item.children = deepCreateNode(item.children, objForSplice, startSplice);
      }
      return item;
    });
  } else {
    return [...deepCloneData, { id: generateId, title: '', children: [] }];
  }
};

const deepEdit = (data: Array<TodoListDataType>, nodeId: string, title: string) => {
  const deepCloneData = JSON.parse(JSON.stringify(data));
  return deepCloneData.map(item => {
    if (item.id === nodeId) {
      item.title = title;
    } else {
      item.children = deepEdit(item.children, nodeId, title);
    }
    return item;
  });
};

const deepRemove = (data: Array<TodoListDataType>, nodeId: string) => {
  const deepCloneData = JSON.parse(JSON.stringify(data));
  return deepCloneData.filter(item => {
    if (item.children) {
      item.children = deepRemove(item.children, nodeId);
    }
    return item.id != nodeId;
  });
};

const deepIndent = (data: Array<TodoListDataType>, nodeData: NodeAndParentItems) => {
  const deepCloneData = JSON.parse(JSON.stringify(data));
  return deepCloneData.filter(o => {
    if (o.children) o.children = deepIndent(o.children, nodeData);
    if (nodeData.nodeIndex > 0 && o.id === nodeData.nodeObj.id) {
      deepCloneData[nodeData.nodeIndex - 1].children.push(nodeData.nodeObj);
    }
    if (nodeData.nodeIndex <= 0) {
      return deepCloneData;
    } else {
      return o.id != nodeData.nodeObj.id;
    }
  });
};

const deepIndentOutwards = (data: Array<TodoListDataType>, nodeData: NodeAndParentItems, fake: Array<TodoListDataType>) => {
  if (nodeData.parentObj) {
    for (const entry of data) {
      if (entry.id === nodeData.parentObj.id) {
        data.splice(nodeData.parentIndex + 1, 0, nodeData.nodeObj);
        entry.children = entry.children.filter(e => e.id !== nodeData.nodeObj.id);
        return fake;
      }
      const deeperParentId = deepIndentOutwards(entry.children, nodeData, fake);
      if (entry.children && deeperParentId) {
        return deeperParentId;
      }
    }
  } else {
    return fake;
  }
};

export function* setToLocalstorageAndApp(newData: Array<TodoListDataType>) {
  localStorage.setItem('treeData', JSON.stringify(newData));
  yield put(actions.setTodoList(newData));
  return;
}

export function* addNodeTodoList(payload: PayloadDataType) {
  const todoList = yield select(selectTodoList);
  const newData = deepCreateNode(todoList, payload.createData?.nodeObj, payload.createData?.startSplice as number);
  yield call(setToLocalstorageAndApp, newData);
  return;
}

export function* editNodeTodoList(payload: PayloadDataType) {
  const todoList = yield select(selectTodoList);
  if (payload.nodeId && payload.title) {
    const newData = yield call(deepEdit, todoList, payload.nodeId, payload.title);
    yield call(setToLocalstorageAndApp, newData);
  }
  return;
}

export function* deleteNodeTodoList(payload: PayloadDataType) {
  const todoList = yield select(selectTodoList);
  if (payload.nodeId) {
    const newData = deepRemove(todoList, payload.nodeId);
    yield call(setToLocalstorageAndApp, newData);
  }
  return;
}

export function* indentNodeTodoList(payload: PayloadDataType) {
  const todoList = yield select(selectTodoList);
  if (payload.nodeData) {
    const newData = deepIndent(todoList, payload.nodeData);
    yield call(setToLocalstorageAndApp, newData);
  }
  return;
}

export function* indentOutwardsNodeTodoList(payload: PayloadDataType) {
  const todoList = yield select(selectTodoList);
  const deepCloneData = JSON.parse(JSON.stringify(todoList));
  if (payload.nodeData) {
    const newData = deepIndentOutwards(deepCloneData, payload.nodeData, deepCloneData);
    yield call(setToLocalstorageAndApp, [...newData]);
  }
  return;
}

export function* todoListActions({ payload }: { payload: PayloadDataType }) {
  switch (payload.action) {
    case 'create' :
      yield call(addNodeTodoList, payload);
      break;
    case 'edit' :
      yield call(editNodeTodoList, payload);
      break;
    case 'delete' :
      yield call(deleteNodeTodoList, payload);
      break;
    case 'indent' :
      yield call(indentNodeTodoList, payload);
      break;
    case 'indentOutwards' :
      yield call(indentOutwardsNodeTodoList, payload);
      break;
  }
  return;
}

/**
 * Root saga manages watcher lifecycle
 */
export function* todoListSaga() {
  yield takeLatest(actions.todoListActions, todoListActions);
}
