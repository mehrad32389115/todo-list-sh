export const tokens = {
  colors: {
    teal10: '#00A3BE',
    teal20: '#05778F',
    gray10: '#F9FAFC',
    gray20: '#ECEEF4',
    gray30: '#CDD0E0',
    gray50: '#6E7492',
    gray90: '#191D2F',
  },
  font:'-apple-system, "Segoe UI", Roboto, Arial, sans-serif',
  fontSizes: {
    m: '16px',
    s: '12px',
  },
  fontWeights: {
    regular: 400,
    medium: 500,
    bold: 700,
  },
  space:{
    s5:'4px',
    s10:'8px',
    s15:'12px',
    s20:'16px',
    s25:'20px',
    s30:'24px',
    s40:'32px',
    s50:'40px',
    s60:'48px',
    s70:'56px',
  },
  boxShadow:"0px 10px 5px 0px #6e7492"
};

const lightTheme = {
  primary: tokens.colors.teal10,
}
const darkTheme = {
  primary: tokens.colors.gray10,
}

export const themes = {
  light: {...tokens,...lightTheme},
  dark: {...tokens,...darkTheme}
};
export type Theme = typeof themes;


export default themes